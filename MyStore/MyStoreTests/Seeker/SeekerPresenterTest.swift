//
//  SeekerPresenterTest.swift
//  MyStoreTests
//
//  Created by iMac on 29/03/21.
//

import XCTest
@testable import MyStore

class SeekerPresenterTests: XCTestCase {

    var mockView: MockView!
    var mockRouter: MockRouter!
    var mockInteractor: MockInteractor!
    var sut: SeekerPresenter!
    
    override func setUp(){
        super.setUp()
        mockView = MockView()
        mockRouter = MockRouter()
        mockInteractor = MockInteractor()
        sut = SeekerPresenter()
        sut.view = mockView
        sut.router = mockRouter
        sut.interactor = mockInteractor
    }
    
    override func tearDown() {
        super.tearDown()
        mockView = nil
        mockRouter = nil
        mockInteractor = nil
        sut = nil
    }
    
    func test_validateQuery(){
        //Given
        
        //When
        sut.validateQuery(text: "")
        //Then
        XCTAssertTrue(mockView.validateQueryCalled)
    }
    
    
    
    func test_goToHome(){
        //Given
        
        //When
        sut.goToHome(query: "", view: HomeViewController())
        //Then
        XCTAssertTrue(mockRouter.disapearToHomeCalled)
        
    }
    
    class MockView: PresenterToViewSeekerProtocol {
        
        var validateQueryCalled = false
        
        func validateQuery(isValid: Bool) {
            validateQueryCalled = true
        }
        
        
    }
    
    class MockRouter: PresenterToRouterSeekerProtocol{
        
        var createModuleCalled = false
        var disapearToHomeCalled = false
        
        static func createModule(delegate: PresenterToPresenterSeekerProtocol) -> SeekerViewController {
            
            SeekerViewController()
            //createModuleCalled = true
        }
        
        func disapearToHome(query: String, view: UIViewController) {
            
            disapearToHomeCalled = true
            
        }
        
        
    }
    
    class MockInteractor: PresenterToInteractorSeekerProtocol {
        
        var presenter: InteractorToPresenterSeekerProtocol?
        
    }
}


//
//  ProductDetailTest.swift
//  MyStoreTests
//
//  Created by iMac on 29/03/21.
//

import XCTest
@testable import MyStore

class ProductDetailPresenterTests: XCTestCase {

    var mockView: MockView!
    var mockRouter: MockRouter!
    var mockInteractor: MockInteractor!
    var sut: ProductDetailPresenter!
    
    override func setUp(){
        super.setUp()
        mockView = MockView()
        mockRouter = MockRouter()
        mockInteractor = MockInteractor()
        sut = ProductDetailPresenter()
        sut.view = mockView
        sut.router = mockRouter
        sut.interactor = mockInteractor
    }
    
    override func tearDown() {
        super.tearDown()
        mockView = nil
        mockRouter = nil
        mockInteractor = nil
        sut = nil
    }
    
    func test_getProductDetail(){
        //Given
        
        //When
        sut.getProductDetail()
        //Then
        XCTAssertTrue(mockView.getProductCalled)
    }
    
    func test_getRowsTable(){
        //Given
        
        //When
        sut.getRowsTable()
        //Then
        XCTAssertTrue(mockView.getDataTableCalled)
    }
    
    
    class MockView: PresenterToViewProductDetailProtocol {
        
        var getProductCalled = false
        var getDataTableCalled = false
        
        func getProduct(product: ProductModel?) {
            
            getProductCalled = true
            
        }
        
        func getDataTable(container: [ProductDetailCellModel]) {
            
            getDataTableCalled = true
            
        }
        
        
    }
    
    class MockRouter: PresenterToRouterProductDetailProtocol{
        
        var createModuleCalled = false
        
        static func createModule(product: ProductModel) -> ProductDetailViewController {
            
            ProductDetailViewController()
            
        }
        
    }
    
    class MockInteractor: PresenterToInteractorProductDetailProtocol {
        
        var presenter: InteractorToPresenterProductDetailProtocol?
        
    }
}



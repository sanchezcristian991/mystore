//
//  SeekerProtocols.swift
//  MyStore
//
//  Created by iMac on 27/03/21.
//

import Foundation
import UIKit
protocol ViewToPresenterSeekerProtocol : class {
    
    var view: PresenterToViewSeekerProtocol? {get set}
    var interactor: PresenterToInteractorSeekerProtocol? {get set}
    var router: PresenterToRouterSeekerProtocol? {get set}
    var delegate: PresenterToPresenterSeekerProtocol? {get set}
    
    func validateQuery(text: String)
    func goToHome(query: String, view: UIViewController)
}

protocol PresenterToViewSeekerProtocol: class {
    
    func validateQuery(isValid: Bool)
    
}

protocol PresenterToRouterSeekerProtocol: class {
    
    static func createModule(delegate: PresenterToPresenterSeekerProtocol) -> SeekerViewController
    func disapearToHome(query: String, view: UIViewController)
    
    
}

protocol PresenterToInteractorSeekerProtocol: class {
    
    var presenter: InteractorToPresenterSeekerProtocol? {get set}
    
    
}

protocol InteractorToPresenterSeekerProtocol: class {
    
}

protocol PresenterToPresenterSeekerProtocol: class {
    
    func dissmiss( query: String)
    
}

//
//  SeekerViewController.swift
//  MyStore
//
//  Created by iMac on 27/03/21.
//

import UIKit

class SeekerViewController: UIViewController {
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    
    var presenter: ViewToPresenterSeekerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        // Do any additional setup after loading the view.
    }
    
    /** this function configure the componets in the seerker view */
    func setUpView(){
        presenter?.validateQuery(text: "")
        btnSearch.addTarget(self, action: #selector(tapSearch), for: .touchDown)
        txtSearch.addTarget(self, action: #selector(changueTxt(textfield :)), for: .editingChanged)
       
    }
    
    /** excecute method for go to home module and pass query words*/
    @objc func tapSearch(){
        presenter?.goToHome(query: txtSearch.text!, view: self)
    }
    /**
        this function bind the textfield for later excecute validate query method
        :params: textfield seeker
     */
    @objc func changueTxt( textfield: UITextField){
        presenter?.validateQuery(text: textfield.text!)
    }
    /**
        this method changue color and  property  button (enabled)
     */
    func enableOrDisableButton(isEnabled: Bool){
        
        btnSearch.isEnabled = isEnabled
        btnSearch.backgroundColor = (isEnabled ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) : #colorLiteral(red: 0.4274014533, green: 0.4274794757, blue: 0.4273965359, alpha: 1))
        
    }
    
    
      
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SeekerViewController: PresenterToViewSeekerProtocol{
    
    func dissmiss() {
        
    }
    
    func validateQuery(isValid: Bool) {
        
        enableOrDisableButton(isEnabled: isValid)
        
    }
    
    
}

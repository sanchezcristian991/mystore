//
//  AttributesModel.swift
//  MyStore
//
//  Created by iMac on 28/03/21.
//

import Foundation
struct  AttributesModel: Codable{
    
    var id: String?
    var value_id: String?
    var attribute_group_name: String?
    var source: Int
    var name: String?
    var value_name: String?
    var attribute_group_id: String?

}

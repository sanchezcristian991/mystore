//
//  ProductDetailProtocols.swift
//  MyStore
//
//  Created by iMac on 28/03/21.
//

import Foundation

protocol ViewToPresenterProductDetailProtocol : class {
    
    var view: PresenterToViewProductDetailProtocol? {get set}
    var interactor: PresenterToInteractorProductDetailProtocol? {get set}
    var router: PresenterToRouterProductDetailProtocol? {get set}
    var product: ProductModel? {get set}
    
    func getProductDetail()
    func getRowsTable()
}

protocol PresenterToViewProductDetailProtocol: class {
    
    func getProduct(product: ProductModel?)
    func getDataTable(container: [ProductDetailCellModel])
}

protocol PresenterToRouterProductDetailProtocol: class {
    
    static func createModule(product: ProductModel)->ProductDetailViewController
    
}

protocol PresenterToInteractorProductDetailProtocol: class {
    
    var presenter:InteractorToPresenterProductDetailProtocol? {get set}
    
    
}

protocol InteractorToPresenterProductDetailProtocol: class {
    
}

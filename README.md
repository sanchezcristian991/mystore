# Prueba para mercado libre por (Anrey Carreño).
### Swift V.5 

## Table of Contents

* [Introducción](#Introducción)
* [Arquitectura](#naming)
  
  * [Delegates](#delegates)
  * [Use Type Inferred Context](#use-type-inferred-context)
  

### Introducción 

Buenas noches, está es mi prueba espero que disculpen era la primera vez que usaba viper espero les guste muchas gracias.

## Artuitectura

La Arquitectura seleccionada fue VIPER por su des acople y facilidad para hacer pruebas

### Delegates

Se podran encontrar los siguientes delegados en la prueba por modulos

**Home**:
**ViewToPresenterHomeProtocol**
```swift
    var view: PresenterToViewHomeProtocol? {get set}
    var interactor: PresenterToInteractorHomeProtocol? {get set}
    var router: PresenterToRouterHomeProtocol? {get set}
    func startFetchingProducts(query: String)
    func openSeeker(navigationController: UINavigationController)
    func showProducDetail(product: ProductModel, navigation: UINavigationController)
```
**PresenterToViewHomeProtocol**
```swift
    func onProductResponseSuccess(producList: Array<ProductModel>)
    func onMovieResponseFailed(error: String)
```
**PresenterToRouterHomeProtocol**
```swift
    static func createModule()->HomeViewController
    func openSeeker(delegate: HomePresenter, navigationController: UINavigationController)
    func pushProductDetail(product: ProductModel, navigationController: UINavigationController)
```
**PresenterToInteractorHomeProtocol**
```swift
    var presenter:InteractorToPresenterHomeProtocol? {get set}
    func fetchProducts(query: String)
```
**Buscador**:
**ViewToPresenterSeekerProtocol**
```swift
    var view: PresenterToViewSeekerProtocol? {get set}
    var interactor: PresenterToInteractorSeekerProtocol? {get set}
    var router: PresenterToRouterSeekerProtocol? {get set}
    var delegate: PresenterToPresenterSeekerProtocol? {get set}
    
    func validateQuery(text: String)
    func goToHome(query: String, view: UIViewController)
```
**PresenterToViewSeekerProtocol**
```swift
    func validateQuery(isValid: Bool)
```

**PresenterToRouterSeekerProtocol**
```swift
    static func createModule(delegate: PresenterToPresenterSeekerProtocol) -> SeekerViewController
    func disapearToHome(query: String, view: UIViewController)
```
**PresenterToPresenterSeekerProtocol**
```swift
    func dissmiss( query: String)
```

### Language

El lenguaje usado para hacer la prueba fue ingles

### Protocol Conformance

 Se agregaron  extensiones separadas para los métodos de protocolos. Esto mantiene los métodos relacionados agrupados junto con el protocolo y puede simplificar las instrucciones para agregar un protocolo a una clase con sus métodos asociados.


**Preferred**:
```swift
extension HomeViewController: PresenterToViewHomeProtocol{
}
extension HomePresenter: PresenterToPresenterSeekerProtocol{
}
extension HomePresenter: InteractorToPresenterHomeProtocol{
}
extension HomeInteractor: InteractorToServicesProtocols{
}
extension SeekerViewController: PresenterToViewSeekerProtocol{
}
extension ProductDetailViewController: PresenterToViewProductDetailProtocol{
}
```

